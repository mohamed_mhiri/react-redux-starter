import { FETCH_POSTS, NEW_POST } from '../actions/types';

const initialState = {
    items: [],
    /*
        the single post added
    */
    item: {}
};

/*
    this function
    evaluates 
*/
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_POSTS:
            return {
                items: [
                    ...action.payload,
                    ...state,
                ],
                item: state.item
            };
        case NEW_POST:
            return {
                items: [
                    ...action.payload,
                    ...state,
                ],
                item: action.payload
            };
        default:
            return state;
    }
};