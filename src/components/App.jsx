import React, { Component } from 'react'
import Posts from './Posts.jsx';
import PostForm from './post-form.jsx';
// the main glue
import {Provider} from 'react-redux';
import store from '../store'

export default class App extends Component {
    render() {
        return (
            /* 
                react-redux binds 
                react and redux together
            */
            <Provider store={store}>
                <div>
                    <PostForm />
                    <hr/>
                    <Posts />
                </div>
            </Provider>
        )
    }
}