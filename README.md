# Notes

## Redux Data Flow

![alt text](https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fimage.slidesharecdn.com%2Freduxdataflowwithangular-170623072558%2F95%2Fredux-data-flow-with-angular-24-638.jpg%3Fcb%3D1498202795&f=1 "Redux React")

```View``` is basically a component (button, form, ...) which will fire up an ```Action creators``` which will dispatch an action to the ```Store```.
```State``` is a Javascript object or array.
```Reducers``` which are a pure functions that specify how the application's ```state``` should change in response to that ```action```.
And then the ```Reducers``` respond with a ```new state```.
```State``` is immutable which means it can't be modified. So a ```new state``` will be created.
And in turn that ```state``` get send down to the ```view``` which will react accordingly to that ```state```.

## Flux vs Redux

Redux in action p 7

Redux isn’t part of any existing framework or library, but additional tools called bindings connect Redux with React.

Redux principles:
1. Single Source of Truth:
Redux manages the entire application's state in one object named the ```store```.
This way, developers' experience got simplified; an easy way to thing through the application flow, predict the outcome of new actions and debug issues produced by any given action.

2. State is Read-only:
```State``` is immutable which means it can't be modified. So a ```new state``` will be created in response to ```Reducers```.

3. Changes are made with pure functions:
Pure functions produce the same output given the same inputs, without mutating any data in the process.
`Reducers` are pure functions in `Redux`. And if they were to mutate the existing state, while producing the new one, the new state may be erroneous and the transaction log provided by each new action will be lost.
Thanks to this feature, the `Redux developer` community created so many tools, extensions, ... to manage the state(undo, redo a functionality...).

## Why Redux

1. Predictability:
`Redux Architecture` offers a straightforward way to conceptualize and manage state, through one action at a time.

2. Developer Experience:
`Redux` enables some cutting edge debugging tools.
`Hot-loading` and `time-travel` `debugging` provide developers with super faster development cycles.

3. Testibility:
Many of `Redux`'s functions are pure and easy to get unit-tested.

4. Learning Curve:
`Redux` has a remarkably small footprint, exposing only a handful of API's to get the job done.

5. Size:
`Redux` minified library is under 7KB.

`Redux` `co-creator` `Dan Abramov` posted ```"You Might Not Need Redux."``` He recommends starting without Redux and introducing the library only after you’ve reached enough state management pain points to justify including it.
Smaller applications without complex data requirements are the most common scenario where it might be more appropriate to not use Redux in favor of plain React.

It's highly recommended to use `Redux` if:

1. Data passes through several layers of components that don't have any use for it.

2. Data is shared and synchronised between unrelated parts of the application.

## Alternatives to Redux

Redux in Action p18

`react-redux` is the library which binds them together
`redux-thunk` is a middleware for redux which allows to directly access to the dispatch methods, so that we can make asynchronous calls from actions